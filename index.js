var app = require("express")();
var http = require("http").Server(app);
var io = require('socket.io')(http);
var path = require('path');

app.get("/",function(req,res){
	res.sendFile("index.html",{root: path.join(__dirname,"/")});
});

io.on('connection', function(socket){
  console.log('a user connected');

  socket.on('chat message', function(msg){
  	console.log(msg);
    io.emit('chat message', msg);
  });

  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
});

http.listen(3000, function(){
	console.log("listenin' on *:3000");
});